let reduce_func = function (item,cb){
    let sum = 0

    for (let element of item )
    {
        sum += cb(element)

    }
    return sum
}

module.exports = reduce_func;